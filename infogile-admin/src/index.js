import React from 'react'
import {render} from 'react-dom'
import App from './App'
import {BrowserRouter} from 'react-router-dom'
import {AuthStore} from "./state/AuthStore";
import {AppSpacesStore} from "./state/AppSpacesStore";
import {CurrentAppSpaceStore} from "./state/CurrentAppSpaceStore";
import {Infogile, getUser} from "infogile-api";


const federated = {
    google_client_id: '997286163728-i5k06rhev0olujbi7kp3frrbftb168bi.apps.googleusercontent.com',
};

window.stores = window.stores || {
    auth: new AuthStore(),
    appspaces: new AppSpacesStore(),
    appspace: new CurrentAppSpaceStore()
};

if (!window.subscription) {
    Infogile.openWebsocket(websocket => {
        console.log("Opened websocket ", websocket);
        // window.subscription = websocket;
    });
}

const root = document.createElement('div');
document.body.appendChild(root);
init();


async function init() {

    console.log("1")
    let accessToken = window.localStorage.getItem("InfogileAccessToken");
    let identityProvider = window.localStorage.getItem("InfogileIdentityProvider");
    Infogile.init({
        idTokenProvider: () => {
            return {
                accessToken: window.localStorage.getItem("InfogileAccessToken"),
                identityProvider: window.localStorage.getItem("InfogileIdentityProvider")
            }
        },
        sessionTimeStart: Date.now()
    });
    getUser(identityProvider, accessToken)
        .then(user => {
            window.stores.auth.setUser(user);
            render(
                <BrowserRouter>
                    <App stores={window.stores}/>
                </BrowserRouter>, root);
        })
        .catch(err => {
            render(
                <BrowserRouter>
                    <App stores={window.stores}/>
                </BrowserRouter>, root);

        });
}
