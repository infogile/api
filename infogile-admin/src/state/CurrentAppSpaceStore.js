import {action, observable} from "mobx";
import {Infogile} from "infogile-api";
import Cache from "../util/Cache";


export class CurrentAppSpaceStore {

    @observable objectDefinitions = [];
    @observable currentObjectDefinition = null;
    @observable currentObjectDefinitionAppSpace = null;
    @observable objectDefinitionInSaveProgress = false;
    @observable currentObjectDefinitionData = null;
    @observable referenceCache = new Cache(5000, (key) => {
        return Infogile.listData({
            appSpaceIdentity: this.currentObjectDefinitionAppSpace,
            objectDefinitionIdentity: key
        });
    });
    filter = null;

    @action.bound
    setObjectDefinitions(objectDefinitions) {
        this.objectDefinitions = objectDefinitions;
    }

    @action.bound
    setCurrentObjectDefinition(objectDefinition) {
        this.currentObjectDefinition = objectDefinition;
        Infogile.subscribe(this.currentObjectDefinitionAppSpace, objectDefinition.identity, update => {

            Infogile.listData({
                appSpaceIdentity: update.appSpaceIdentity,
                objectDefinitionIdentity: update.objectDefinitionIdentity,
                filter: this.filter
            })
                .then(result => {
                    this.referenceCache.put(objectDefinition.identity, result.data);
                    this.setCurrentObjectDefinitionData(result.data)
                })
        })
            .catch(err => {
                console.error(err);
            })
    }

    setCurrentObjectDefinitionAppSpace(appSpace) {
        this.currentObjectDefinitionAppSpace = appSpace;
    }

    setCurrentObjectDefinitionData(data) {
        this.currentObjectDefinitionData = data;
    }

    createDummy(appSpaceIdentity) {
        Infogile.createDummyObjectDefinition(appSpaceIdentity)
            .then(() => {
                return this.listObjectDefinitions(appSpaceIdentity)
            })
    }

    newObjectDefinition(appSpaceIdentity) {
        this.setCurrentObjectDefinition({fields: []});
        this.setCurrentObjectDefinitionAppSpace(appSpaceIdentity);
    }

    deleteObjectDefinition(identity) {
        Infogile.deleteObjectDefinition(appSpaceIdentity, identity)
            .then(() => {
                this.referenceCache.purge(identity);
                return this.listObjectDefinition(appSpaceIdentity);
            })
    }

    listObjectDefinitions(appSpaceIdentity) {
        this.setObjectDefinitions([]);
        Infogile.objectDefinitions(appSpaceIdentity)
            .then(objectDefinitions => {
                this.setObjectDefinitions(objectDefinitions);
            })
    }

    objectDefinition(appSpaceIdentity, objectDefinitionIdentity) {
        return Infogile.objectDefinition(appSpaceIdentity, objectDefinitionIdentity)
            .then(objectDefinition => {
                this.setCurrentObjectDefinitionAppSpace(appSpaceIdentity);
                this.setCurrentObjectDefinition(objectDefinition);
            })
    }

    saveCurrentObjectDefinition() {
        this.objectDefinitionInSaveProgress = true;
        Infogile.saveObjectDefinition(this.currentObjectDefinitionAppSpace, this.currentObjectDefinition)
            .then(c => {
                this.listObjectDefinitions(this.currentObjectDefinitionAppSpace);
                this.objectDefinition(this.currentObjectDefinitionAppSpace, c.identity)
                    .then(() => {
                        this.objectDefinitionInSaveProgress = false;
                    });
            });
    }

    objectDefinitionData(appSpaceIdentity, objectDefinitionIdentity, filter) {
        this.filter = filter;

        return Infogile.objectDefinition(appSpaceIdentity, objectDefinitionIdentity)
            .then(objectDefinition => {
                this.setCurrentObjectDefinitionAppSpace(appSpaceIdentity);
                this.setCurrentObjectDefinition(objectDefinition);
                Infogile.listData({appSpaceIdentity, objectDefinitionIdentity: objectDefinitionIdentity, filter})
                    .then(result => {
                        this.setCurrentObjectDefinitionData(result.data)
                    })
            })
    }

    saveItem(item) {
        if (!this.currentObjectDefinitionAppSpace) {
            console.error("There are no current appSpace!");
            return;
        }
        Infogile.saveItem(this.currentObjectDefinitionAppSpace, item)
            .then(() => {
                this.referenceCache.purge(item.type);
            })
    }

    deleteItem(item) {
        if (!this.currentObjectDefinitionAppSpace) {
            console.error("There are no current appSpace!");
            return;
        }
        Infogile.deleteItems(this.currentObjectDefinitionAppSpace, item)
            .then(() => {
                // Infogile.listData({appSpaceIdentity: this.currentObjectDefinitionAppSpace, objectDefinitionIdentity: item.type})
                //     .then(result => {
                //         this.referenceCache.put(item.type, result.data);
                //         this.setCurrentObjectDefinitionData(result.data)
                //     })
            })
    }

    references(apiName) {

    }
}