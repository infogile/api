export default class User {
    email = null;
    firstName = null;
    lastName = null;
    accessToken = null;
    identityProvider = null;
}