import {action, observable} from "mobx";
import {createAppSpace, getAppSpaces} from "infogile-api";

export class AppSpacesStore {
    @observable appSpaces = [];

    @action.bound
    setAppSpaces(appSpaces) {

        console.log("setting appspaces to ", appSpaces);
        this.appSpaces = appSpaces;
    }

    listAppSpaces(authenticationOptions) {
        console.log("listAppSpaces(", authenticationOptions, ")");
        getAppSpaces(authenticationOptions)
            .then(res => this.setAppSpaces(res))
            .catch(err => console.error(err));
    }

    createAppSpace(name, authenticationOptions) {
        console.log("createAppSpace(", name, ", ", authenticationOptions, ")");
        createAppSpace(name, authenticationOptions)
            .then(res => this.listAppSpaces(authenticationOptions))
            .catch(err => console.error(err));
    }

}