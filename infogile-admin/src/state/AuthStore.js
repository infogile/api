import {action, observable} from "mobx";
import {getUser} from "infogile-api";

export class AuthStore {
    @observable user = null;

    @action.bound
    setUser(user) {
        this.user = user;
        window.localStorage.setItem("InfogileAccessToken", user.accessToken);
        window.localStorage.setItem("InfogileIdentityProvider", user.identityProvider);
    }

    signOut() {
        this.user = null;
        window.localStorage.removeItem("InfogileAccessToken");
        window.localStorage.removeItem("InfogileIdentityProvider");
    }

    signIn(identityProvider, accessToken) {
        return new Promise((resolve, reject) => {
            let self = this;
            console.log("signIn(" + identityProvider + ", " + accessToken + ")");
            getUser(identityProvider, accessToken)
                .then(user => {
                    console.log("Got User!", user);
                    self.setUser(user);
                    resolve();
                })
                .catch(err => {
                    console.warn("Error while validating login", err);
                    reject();
                })
        })

    }
}