import {uuidv4} from "infogile-api";

export default class Cache {
    constructor(ttl, loader, evictionListener) {
        this.ttl = ttl;
        this.loader = loader;
        this.evictionListener = evictionListener;
        this.cache = {};
        this.loadingInProgress = {};
    }

    purge(key) {
        delete this.cache[key];
    }

    put(key, value) {
        let item = new CacheItem(value, this.ttl);
        this.cache[key] = item;
    }

    get(key) {
        let result;
        let item = this.cache[key];

        if (!item && this.loader) {
            if (this.loadingInProgress[key] && Object.keys(this.loadingInProgress[key]).length > 0) {
                // Måste ta bort loadingInProgress-funktionen när den väl har hämtat datat!!!
                let loaderUuid = uuidv4();
                result = new Promise(resolve => {
                    this.loadingInProgress[key][loaderUuid] = (d) => {
                        resolve(d);
                        delete this.loadingInProgress[key][loaderUuid];
                    };
                });

            } else {
                this.loadingInProgress[key] = {};
                let object = this.loader(key);
                result = Promise.resolve(object);
                result.then(o => {
                    Object.keys(this.loadingInProgress[key]).forEach(k => {
                        let f = this.loadingInProgress[key][k];
                        f(o);
                    });
                    if (o != null) {
                        this.put(key, o);
                    }
                })
            }
        } else if (item != null && item.timeout < Date.now()) {
            delete this.cache[key];
            if (this.evictionListener) {
                this.evictionListener(key, item);
            }
            return this.get(key);
        } else {
            result = new Promise(resolve => resolve(item.object));
        }
        return result;


        if (!item && this.loader) {
            let object = this.loader(key);
            result = Promise.resolve(object);
            result.then(o => {
                if (o != null) {
                    this.put(key, o);
                }
            })
        } else if (item != null && item.timeout < Date.now()) {
            delete this.cache[key];
            if (this.evictionListener) {
                this.evictionListener(key, item);
            }
            result = new Promise(resolve => resolve(null));
        } else {
            result = new Promise(resolve => resolve(item.object));
        }
        return result;
    }
}

class CacheItem {
    constructor(object, ttl) {
        this.object = object;
        this.timeout = Date.now() + ttl;
    }
}
