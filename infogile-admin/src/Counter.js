import React from "react";

class Counter extends React.Component {
    constructor() {
        super();
        this.state = {count: 0}
    }

    componentDidMount() {
        this.interval = setInterval(
            () => this.setState(prevState => ({count: prevState.count + 1})),
            200,
        )
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        return <div>16:{this.state.count}</div>
    }
}

export default Counter