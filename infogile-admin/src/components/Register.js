import React from "react";
import {withRouter} from "react-router-dom";

class Register extends React.Component {
    constructor() {
        super();
        this.state = {givenName: "", familyName: "", password1: "", password2: "", email: ""};
    }

    componentWillMount() {
    }

    register() {
       console.error("NOT IMPLEMENTED!");
    }

    render() {
        return (
            <div>
                <h2>Password reset ({this.state.code})</h2>
                <div>
                    <div>
                        <h5>Email</h5>
                        <div>
                            <input
                                type="email"
                                onChange={(e) => this.setState({email: e.target.value})}
                                value={this.state.email}/>
                        </div>
                        <h5>Given name</h5>
                        <div>
                            <input
                                type="text"
                                onChange={(e) => this.setState({givenName: e.target.value})}
                                value={this.state.givenName}/>
                        </div>
                        <h5>Family name</h5>
                        <div>
                            <input
                                type="text"
                                onChange={(e) => this.setState({familyName: e.target.value})}
                                value={this.state.familyName}/>
                        </div>
                        <h5>Password</h5>
                        <div>
                            <input
                                type="password"
                                onChange={(e) => this.setState({password1: e.target.value})}
                                value={this.state.password1}/>
                        </div>
                        <h5>Confirm new password</h5>
                        <div>
                            <input
                                type="password"
                                onChange={(e) => this.setState({password2: e.target.value})}
                                value={this.state.password2}/>
                        </div>
                        <button onClick={this.register.bind(this)}>Register</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Register);