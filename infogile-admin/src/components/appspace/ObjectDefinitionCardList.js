import React from "react";
import {inject, observer} from "mobx-react";
import {Card, Icon, Input} from "antd";
import "./appspacelist.scss";
import {withRouter} from "react-router-dom";

@inject("appSpaceStore")
@observer
class ObjectDefinitionCardList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {filter: ""}
    }

    doFilter(e) {
        this.setState({filter: e.target.value})
    }

    editClick(e, identity) {

    }

    render() {
        let appSpaceStore = this.props.appSpaceStore;
        console.log("appSpaceStore.objectDefinitions", appSpaceStore.objectDefinitions);
        return (
            <div className="appspace-list-container">
                <div>
                    <Input placeholder="Filter" value={this.state.filter} onChange={this.doFilter.bind(this)}/>
                </div>
                <div className="appspace-list">
                    {appSpaceStore.objectDefinitions
                        .filter(c => {
                            return c.name.toLowerCase().startsWith(this.state.filter.toLowerCase())
                        })
                        .map(c => {
                            return (
                                <Card key={c.identity}
                                      title={c.name}
                                      style={{width: 400}}
                                      actions={[
                                          <Icon
                                              onClick={() => this.props.history.push("/data/" + this.props.match.params.identity + "/" + c.identity)}
                                              type="eye"/>,
                                          <Icon type="edit"
                                                onClick={() => {
                                                    appSpaceStore.objectDefinition(this.props.match.params.identity, c.identity);
                                                    this.props.history.push("/appspaces/" + this.props.match.params.identity + "/" + c.identity)
                                                }}/>,
                                          <Icon onClick={() => appSpaceStore.deleteObjectDefinition(c.identity)}
                                                type="delete"/>
                                      ]}
                                >
                                    <div>
                                        <p>{c.description || <i>No description</i>}</p>
                                        <div><b>Fields</b>
                                            <div>
                                                <ul>
                                                    {c.fields.map(f => {
                                                        let mandatory;
                                                        if (f.mandatory === true) {
                                                            mandatory = <span style={{color: "red"}}>*</span>
                                                        }
                                                        return (
                                                            <li key={f.identity}>{f.name}{mandatory}</li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="spacer">&nbsp;</div>
                                    <div>
                                        <table className="fine-print">
                                            <tr>
                                                <th>API name:</th>
                                                <td>{c.apiName}</td>
                                            </tr>
                                            <tr>
                                                <th>Identity:</th>
                                                <td>{c.identity}</td>
                                            </tr>
                                        </table>
                                    </div>

                                </Card>
                            )
                        })}
                </div>
            </div>
        )
    }
}

export default withRouter(ObjectDefinitionCardList)