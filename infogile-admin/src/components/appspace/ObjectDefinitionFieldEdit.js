import React from 'react';

import {Checkbox, Dropdown, Icon, Input, Menu, Select} from "antd";
import {inject, observer} from "mobx-react";
import {Droppable, Draggable} from 'react-beautiful-dnd';


@inject("appSpaceStore")
@observer
class ObjectDefinitionFieldEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {fieldEllipsisOpen: {}, draggedField: null, isOver: false, isDragged: false, dragLeft: false}
    }

    toggleFieldEllipsis(fieldIdentity) {
        let fieldEllipsisOpen = Object.assign({}, this.state.fieldEllipsisOpen);
        fieldEllipsisOpen[fieldIdentity] = !fieldEllipsisOpen[fieldIdentity];
        this.setState({fieldEllipsisOpen})
    }

    toggleMandatory(fieldApiName) {
        let field = this.props.appSpaceStore.currentObjectDefinition.fields.filter(f => f.apiName === fieldApiName)[0];
        field.mandatory = !field.mandatory;
    }

    toggleMultiplicity(fieldApiName) {
        let field = this.props.appSpaceStore.currentObjectDefinition.fields.filter(f => f.apiName === fieldApiName)[0];
        if (field.multiplicity === "OneToMany") {
            field.multiplicity = "OneToOne";
        } else {
            field.multiplicity = "OneToMany";
        }
        console.log("Changed mandatory to ", field.multiplicity);
    }

    deleteField(fieldApiName) {
        this.props.appSpaceStore.currentObjectDefinition.fields = this.props.appSpaceStore.currentObjectDefinition.fields.filter(f => f.apiName !== fieldApiName);
    }


    render() {
        const Option = Select.Option;
        // Your component receives its own props as usual
        const {id} = this.props;

        // These props are injected by React DnD,
        // as defined by your `collect` function above:
        const {isDragging, connectDragSource, connectDropTarget} = this.props;
        let f = this.props.field;

        let objectDefinitions = this.props.appSpaceStore.objectDefinitions;


        let menu, moreStuff, fieldTypeComponent, deleteOption;
        if (f.systemObject !== true) {
            deleteOption = (
                <Menu.Item key="delete" className="ellipsis-menu-item"
                           onClick={this.deleteField.bind(this, f.apiName)}
                >
                    <div><Icon type="delete"/></div>
                    <div>Delete Field</div>
                </Menu.Item>
            );
        }
        let menuItems = [
            <Menu.Item key="mandatory" className="ellipsis-menu-item">
                <div>Mandatory</div>
                <div><Checkbox onChange={this.toggleMandatory.bind(this, f.apiName)}
                               checked={f.mandatory === true}/></div>
            </Menu.Item>
            ,
            deleteOption
        ];

        if (f.fieldType === "ValueField") {
            fieldTypeComponent = (
                <div className="field-input value-type">
                    <Select placeholder="Value type" value={f.type}
                            onChange={(e) => f.type = e}>
                        <Option value="Numeric">Number</Option>
                        <Option value="Text">String</Option>
                        <Option value="Boolean">True/false</Option>
                        <Option value="Datetime">Date</Option>
                        <Option value="Binary">Binary</Option>
                    </Select>
                </div>
            )
        } else {
            fieldTypeComponent = (
                <div className="field-input referenced-type">
                    <Select placeholder="Referenced type "
                            onChange={(e) => f.referencedType = e}
                            value={f.referencedType}>
                        {objectDefinitions.map(c => {
                            return <Option key={c.identity}
                                           value={c.apiName}>{c.name}</Option>
                        })}
                    </Select>
                </div>
            );
            menuItems.unshift(
                <Menu.Item key="multiple" className="ellipsis-menu-item">
                    <div>Multiple values</div>
                    <div><Checkbox
                        checked={f.multiplicity === "OneToMany"}
                        onChange={this.toggleMultiplicity.bind(this, f.apiName)}
                    /></div>
                </Menu.Item>
            );

        }
        menu = (
            <Menu className="ellipsis-menu">
                {menuItems}
            </Menu>
        );

        moreStuff = (
            <Dropdown overlay={menu}
                      onVisibleChange={this.toggleFieldEllipsis.bind(this, f.apiName)}
                      visible={this.state.fieldEllipsisOpen[f.apiName]}
                      trigger={['click']}>
                <Icon type="ellipsis"/>
            </Dropdown>
        );

        let result = (
            <div className={"objectdefinition-field-entry-container"}>
                <div key={f.identity || this.props.position} className="objectdefinition-field-entry">

                    <div {...this.props.dragHandleProps} className="drag-handle">
                        <Icon type="drag"/>
                    </div>
                    <div className="field-input field-name">
                        <Input placeholder="Field name" value={f.name}
                               onChange={(e) => f.name = e.target.value}/>
                    </div>
                    <div className="field-input api-name">
                        <Input placeholder="API name" value={f.apiName}
                               onChange={(e) => f.apiName = e.target.value}/>
                    </div>
                    <div className="field-input field-type">
                        <Select placeholder="Field type" value={f.fieldType}
                                onChange={(e) => f.fieldType = e}>
                            <Option value="ValueField">Value</Option>
                            <Option value="ReferenceField">Reference</Option>
                        </Select>
                    </div>
                    {fieldTypeComponent}
                    <div className="ellipsis-field">
                        {moreStuff}
                    </div>
                </div>
            </div>
        );
        return result;

    }
}


export default ObjectDefinitionFieldEdit;