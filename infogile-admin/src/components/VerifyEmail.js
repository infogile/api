import React from "react";
import {withRouter} from "react-router-dom";

class Register extends React.Component {
    constructor() {
        super();
        this.state = {result: null};
    }

    componentWillMount() {
    }

    render() {
        return (
            <div>
                <h2>User verification: {this.state.result}</h2>
            </div>
        )
    }
}

export default withRouter(Register);