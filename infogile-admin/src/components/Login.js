import React from "react";
import {Button, Form, Input, Modal} from "antd";
import "./LoginStyle.scss";
import {inject, observer} from 'mobx-react';
import {Infogile} from "infogile-api";
import GoogleLogin from 'react-google-login';
import PropTypes from "prop-types";

@inject("authStore")
@observer
export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {email: "", password: "", modalVisible: false, loginProgress: false, error: null}
    }

    changeValue(type, e) {
        let newState = {};
        newState[type] = e.target.value;
        this.setState(newState);
    }


    login() {
        console.error("NOT IMPLEMENTED!");
    }

    logout() {
        this.props.authStore.signOut();
        const ga = window.gapi.auth2.getAuthInstance();
        ga.signOut()
            .then(a => {
                console.log("Signed out from google!", a);
            });

    }

    componentDidMount() {
        this.createScript()
            .then(() => {
                this.loadCurrentUser();
                setInterval(() => {
                    // console.log("gauth: ", gauth);
                    this.loadCurrentUser();
                }, 30000);
            });

    }

    loadCurrentUser() {
        let self = this;
        if (!this.props.authStore.user) {
            this.props.authStore.signIn("google", window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token)
                .then(() => {
                    self.setState({modalVisible: false, loginProgress: false})
                })
                .catch(() => {
                    self.setState({loginProgress: false, error: "Could not login!"});
                });
        }
    }

    googleSignIn() {
        const ga = window.gapi.auth2.getAuthInstance();
        let self = this;
        this.setState({loginProgress: true})
        ga.signIn().then(
            googleUser => {
                console.log(googleUser);
                console.log("id", googleUser.getId());
                console.log("basic profile", googleUser.getBasicProfile());
                let options = {
                    token: googleUser.getAuthResponse().access_token,
                    expires_at: googleUser.getAuthResponse().expires_at
                };

                this.props.authStore.signIn("google", googleUser.getAuthResponse().access_token)
                    .then(() => {
                        self.setState({modalVisible: false, loginProgress: false})
                    })
                    .catch(() => {
                        self.setState({loginProgress: false, error: "Could not login!"});
                    });


                console.log("googleUser.getAuthResponse().access_token: ", googleUser.getAuthResponse());
            },
            error => {
                console.log(error);
            }
        );
    }


    createScript() {
        // load the Google SDK
        return new Promise((resolve) => {
            if (window.gapi && window.gapi.auth2 ?
                window.gapi.auth2.getAuthInstance() :
                null) {
                resolve();
                return;
            }
            const script = document.createElement('script');
            script.src = 'https://apis.google.com/js/platform.js';
            script.async = true;
            script.onload = () => {
                this.initGapi()
                    .then(() => {
                        resolve();
                    });
            };
            document.body.appendChild(script);
        });
    }

    initGapi() {
        // init the Google SDK client
        return new Promise((resolve, reject) => {
            const g = window.gapi;
            let props = this.props;
            g.load('auth2', function () {
                g.auth2.init({
                    client_id: '997286163728-i5k06rhev0olujbi7kp3frrbftb168bi.apps.googleusercontent.com',
                    // authorized scopes
                    scope: 'profile email openid'
                })
                    .then(() => {
                        resolve();
                    })
                    .catch(() => {
                        reject();
                    });
            });
        });
    }

    getUid() {
        Infogile.uid()
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.error(err);
            })
    }

    responseGoogle(googleUser) {
        let self = this;

        console.log(googleUser);
        console.log("id", googleUser.getId());
        console.log("basic profile", googleUser.getBasicProfile());
        let options = {
            token: googleUser.getAuthResponse().access_token,
            expires_at: googleUser.getAuthResponse().expires_at
        };

        this.props.authStore.signIn("google", googleUser.getAuthResponse().access_token)
            .then(() => {
                self.setState({modalVisible: false, loginProgress: false})
            })
            .catch(() => {
                self.setState({loginProgress: false, error: "Could not login!"});
            });


        console.log("googleUser.getAuthResponse().access_token: ", googleUser.getAuthResponse());

    }

    facebookClicked(e) {
        console.log("facebook clicked: ", e);
    }

    responseFacebook(e) {
        console.log("facebook response: ", e);
    }

    handleAuthStateChange(state) {
        console.log("State change", state);
        if (state === 'signedIn') {
            /* Do something when the user has signed-in */
        }

    }

    toggleModal() {
        this.setState({modalVisible: !this.state.modalVisible})
    }

    openModal() {
        this.setState({modalVisible: true});
    }

    closeModal() {
        this.setState({modalVisible: false});
    }


    //facebook app secret: 5746d6d1341bb7ed5ea4138bc8c3f1b0

    render() {
        let component;
        let user = this.props.authStore.user;
        if (this.props.type === "login") {
            if (user) {
                let nameOfUser = user.givenName + " " + user.familyName;
                component = <div className="signout-wrapper">
                    <div>{nameOfUser}</div>
                    <Button onClick={this.logout.bind(this)}>Sign out</Button></div>
            } else {
                component = <Button onClick={this.openModal.bind(this)}>Sign in!</Button>
            }
        } else {
            if (user) {
                return null;
            }
            component = <div onClick={this.openModal.bind(this)} style={{cursor: "pointer"}}>Register</div>
        }
        //
        return (
            <div className="login-wrapper">
                {component}
                <Modal
                    title={this.props.type === "login" ? "Sign in" : "Register"}
                    visible={this.state.modalVisible}
                    onOk={this.login.bind(this)}
                    onCancel={this.closeModal.bind(this)}
                    footer={[
                        <Button key="back" onClick={this.closeModal.bind(this)}>Cancel</Button>
                    ]}
                >
                    <Form>
                    </Form>
                    <GoogleLogin
                        clientId="997286163728-i5k06rhev0olujbi7kp3frrbftb168bi.apps.googleusercontent.com"
                        buttonText={(this.props.type === "login" ? "Sign in" : "Register") + " with Google"}
                        onSuccess={this.responseGoogle.bind(this)}
                        onFailure={err => console.error(err)}
                        scope="profile email openid"
                        cookiePolicy={'single_host_origin'}
                    />
                </Modal>
            </div>
        )
        //<SignIn federated={myFederatedConfig}/>
        //     <TOTPSetup/>
    }
}

Login.propTypes = {
    type: PropTypes.string.isRequired
};

// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=http://localhost:8081/
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=code&client_id=6d4s6v2g5b64tere4ppjmdaocl&redirect_uri=http://localhost:8081/authcallback
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=https://myDomain.auth.us-east-1.amazoncognito.com