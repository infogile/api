import React from "react";
import PropTypes from "prop-types";
import {Form, Modal} from "antd";
import Field from "./fields/Field";

export default class ItemEditView extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({item: null})
    }

    componentWillMount() {
        this.setState({item: Object.assign({}, this.props.item)});
    }

    render() {
        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };
        return (
            <Modal visible={true}
                   centered={true}
                   onOk={this.props.done.bind(this, this.state.item)}
                   onCancel={this.props.done.bind(this, null)}
                   okText="Save"
                   width={750}
                   title={"Edit Item"}
            >
                <div>
                    <Form>
                        {this.props.objectDefinition.fields.map(f => {
                            return (
                                <Form.Item required={f.mandatory===true} key={f.identity} label={f.name} {...formItemLayout}>
                                    <Field editMode={true} field={f} item={this.props.item}/>
                                </Form.Item>
                            );

                        })}
                    </Form>
                </div>
            </Modal>
        )
    }
}

ItemEditView.propTypes = {
    item: PropTypes.object.isRequired,
    objectDefinition: PropTypes.object.isRequired,
    done: PropTypes.func.isRequired
};