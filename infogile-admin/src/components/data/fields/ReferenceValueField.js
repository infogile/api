import React from "react";
import PropTypes from 'prop-types';
import Field from "./Field";
import {inject, observer} from "mobx-react";
import {Select} from 'antd';

const Option = Select.Option;

@inject("authStore")
@inject("appSpaceStore")
@observer
class ReferenceValueField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {referenceValues: []};
    }

    componentWillMount() {
        this.props.appSpaceStore.referenceCache.get(this.props.field.referencedType)
            .then(result => {
                this.setState({referenceValues: result.data.items});
            })
    }

    changeValue(v) {
        // if (!this.props.item.fields[this.props.field.apiName]) {
        //     this.props.item.fields[this.props.field.apiName] = [];
        // }
        // if (this.props.field.multiplicity === "OneToMany") {
            this.props.item.fields[this.props.field.apiName] = v;
        // }
            // this.props.item.fields[this.props.field.apiName] = v;
        // } else {
        //     this.props.item.fields[this.props.field.apiName].push(v);
        // }
    }

    render() {

        let result = null;

        if (this.props.item && this.props.field && this.state.referenceValues) {
            if (this.props.editMode === true) {
                let select = (
                    <Select onChange={this.changeValue.bind(this)}
                            value={this.props.item.fields[this.props.field.apiName]}
                            mode={this.props.field.multiplicity === "OneToMany" ? "multiple" : "default"}>
                        {this.state.referenceValues.map(r => {
                            return (
                                <Option key={r.identity}
                                        value={r.identity}>{r.fields[Object.keys(r.fields)[0]]}</Option>
                            )
                        })}
                    </Select>
                )
                result = select;
            } else {
                let value = this.props.item.fields[this.props.field.apiName];
                let viewValue = "";
                if (value) {
                    if (!Array.isArray(value)) {
                        value = [value];
                    }
                    value.forEach(v => {
                        let item = this.state.referenceValues.find(r => r.identity === v);
                        if (item) {
                            if (viewValue.length > 0) {
                                viewValue += ", ";
                            }
                            viewValue += item.fields[Object.keys(item.fields)[0]];
                        }
                    })
                }

                result = <div>{viewValue}</div>
            }
        }
        if (!result) {
            result = null;
        }

        return result;
    }
}

ReferenceValueField.propTypes = {
    field: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    editMode: PropTypes.bool
};

export default ReferenceValueField;

Field.addFieldRenderer(ReferenceValueField, f => {
    return f.fieldType === "ReferenceField";
});