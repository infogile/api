import React from "react";
import PropTypes from 'prop-types';
import Field from "./Field";
import {Input} from "antd";
import {inject, observer} from "mobx-react";

@inject("authStore")
@inject("appSpaceStore")
@observer
class TextValueField extends React.Component {

    componentWillMount() {
    }

    render() {
        let result = null;

        if (this.props.item && this.props.field) {
            if (this.props.editMode === true) {
                result = <Input value={this.props.item.fields[this.props.field.apiName]}
                                onChange={(e) => {
                                    this.props.item.fields[this.props.field.apiName] = e.target.value
                                }}
                />
            } else {
                result = this.props.item.fields[this.props.field.apiName]
            }
        }
        if (result === "null") {
            console.log("FUCKING STRING NULL!")
        }
        if (!result) {
            result = "";
        }

        return result;
    }
}

TextValueField.propTypes = {
    field: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    editMode: PropTypes.bool
};

export default TextValueField;

Field.addFieldRenderer(TextValueField, f => {
    return f.fieldType === "ValueField" && f.type === "Text";
});