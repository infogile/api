import React from "react";
import PropTypes from 'prop-types';
import Field from "./Field";
import {Input, DatePicker} from "antd";
import {inject, observer} from "mobx-react";
import Moment from "moment";

@inject("authStore")
@inject("appSpaceStore")
@observer
class DateValueField extends React.Component {

    componentWillMount() {
    }

    render() {
        let result = null;

        if (this.props.item && this.props.field) {
            let v = this.props.item.fields[this.props.field.apiName];
            if (this.props.editMode === true) {
                let value;
                if (v) {
                    value = new Moment(v);
                } else {
                    value = null;
                }
                console.log("Value: ", value);
                result = <DatePicker value={value}
                                     onChange={(e) => {
                                         console.log(e);
                                         this.props.item.fields[this.props.field.apiName] = e.toISOString(false);
                                     }}
                />
            } else {
                result = v ? new Moment(v).format("YYYY-MM-DD") : null;
            }
        }
        if (result === "null") {
            console.log("FUCKING STRING NULL!")
        }
        if (!result) {
            result = "";
        }

        return result;
    }
}

DateValueField.propTypes = {
    field: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    editMode: PropTypes.bool
};

export default DateValueField;

Field.addFieldRenderer(DateValueField, f => {
    return f.fieldType === "ValueField" && f.type === "Datetime";
});