import React from "react";
import PropTypes from 'prop-types';

class Field extends React.Component {

    static fieldRenderers = [];


    static addFieldRenderer(renderer, matcher) {
        this.fieldRenderers.push({matcher, renderer});
    }

    static findFieldRenderer(field) {
        return this.fieldRenderers.find(fr => fr.matcher(field))
    }

    componentWillMount() {
    }

    render() {
        let result = null;

        if (this.props.item && this.props.field) {
            let fieldRenderer = Field.findFieldRenderer(this.props.field);
            if (fieldRenderer) {
                let FieldRenderer = fieldRenderer.renderer;
                result = <FieldRenderer {...this.props}/>
            } else {
                result = this.props.item.fields[this.props.field.apiName];
            }
        }
        if (!result) {
            result = null;
        }

        return result;
    }
}

Field.propTypes = {
    field: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    editMode: PropTypes.bool
};

export default Field;
require("./TextValueField");
require("./ReferenceValueField");
require("./NumberValueField");
require("./DateValueField");
