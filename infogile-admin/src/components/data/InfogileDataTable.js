import React from "react";
import PropTypes from 'prop-types';
import Field from "./fields/Field";
import {inject, observer} from "mobx-react";
import {Button, Icon, Table} from "antd";
import ItemEditView from "./ItemEditView";
import {withRouter} from "react-router-dom";

@inject("appSpaceStore")
@observer
class InfogileDataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {editObject: null}
    }

    openEditWindow(item) {
        this.setState({editObject: item});
    }

    deleteItem(item, e) {
        console.log("delete!");
        this.props.appSpaceStore.deleteItem(item);
        e.stopPropagation();
    }

    saveObject(item) {
        this.props.appSpaceStore.saveItem(item);
    }

    closeEditWindow() {
        this.setState({editObject: null});
    }

    editDone(item) {
        if (item) {
            this.saveObject(item);
        }
        this.closeEditWindow();
    }

    new() {
        let newObject = {
            fields: {},
            type: this.props.data.metadata.objectDefinition.apiName
        };
        newObject = this.props.appSpaceStore.currentObjectDefinitionData.items[this.props.appSpaceStore.currentObjectDefinitionData.items.push(newObject) - 1];
        this.setState({editObject: newObject});
    }

    render() {
        let dataTable, editView;
        if (this.props.data && this.props.data.metadata && this.props.data.metadata.objectDefinition) {
            let fields = this.props.data.metadata.objectDefinition.fields;
            let items = this.props.data.items;

            let columns = fields.map(f => {
                return {
                    title: f.name + " (" + f.apiName + ")",
                    key: f.identity,
                    // rowKey: item => item.identity,
                    render: (item) => {
                        return <Field field={f} item={item}/>
                    }
                }
            });
            columns.unshift({
                title: "Identity",
                key: "identity",
                render: item => {
                    return (
                        <div>{item.identity}</div>
                    )
                }

            })
            columns.push({
                title: "Actions",
                key: "action",
                render: item => {
                    return (
                        <div>
                            <Icon type="delete"
                                  onClick={this.deleteItem.bind(this, item)}/>
                        </div>
                    )
                }
            });
            dataTable = <Table columns={columns}
                               dataSource={items}
                               rowKey="identity"
                               onRow={(record, rowIndex) => {
                                   return {
                                       onClick: this.openEditWindow.bind(this, record),
                                       onDoubleClick: (event) => {
                                       }, // double click row
                                       onContextMenu: (event) => {
                                       },  // right button click row
                                       onMouseEnter: (event) => {
                                       },   // mouse enter row
                                       onMouseLeave: (event) => {
                                       }   // mouse leave row
                                   };
                               }}

                               footer={() => <div style={{textAlign: "right"}}><Button onClick={this.new.bind(this)}
                                                                                       type="primary">New</Button>
                               </div>}
            />;
            if (this.state.editObject) {
                editView = (
                    <ItemEditView item={this.state.editObject}
                                  objectDefinition={this.props.data.metadata.objectDefinition}
                                  done={this.editDone.bind(this)}
                    />
                );
            }
        }
        return (
            <div>

                <div>
                    {editView}
                    {dataTable}
                </div>
            </div>
        )
    }
}

InfogileDataTable.propTypes = {
    data: PropTypes.object.isRequired
};

export default withRouter(InfogileDataTable);