import React from "react";
import {withRouter} from "react-router-dom";
import request from "superagent";

class PasswordReset extends React.Component {
    constructor() {
        super();
        this.state = {pwd1: "", pwd2: "", code: ""};
    }

    componentWillMount() {
        console.log("token", this.props.match.params.token);
        let token = this.props.match.params.token.split(";");
        let email = atob(token[0]);
        let code = token[1];
        this.setState({email, code});
    }

    confirmNewPassword() {
        request.post("/auth/confirmpasswordreset")
            .send({
                email: this.state.email,
                code: this.state.code,
                password: this.state.pwd1
            })
            .then(res => {
                console.log("Result: ", res);
            })
    }

    render() {
        return (
            <div>
                <h2>Password reset ({this.state.code})</h2>
                <div>
                    <div>
                        <h5>New password</h5>
                        <div>
                            <input
                                type="password"
                                onChange={(e) => this.setState({pwd1: e.target.value})}
                                value={this.state.pwd1}/>
                        </div>
                        <h5>Confirm new password</h5>
                        <div>
                            <input
                                type="password"
                                onChange={(e) => this.setState({pwd2: e.target.value})}
                                value={this.state.pwd2}/>
                        </div>
                        <button onClick={this.confirmNewPassword.bind(this)}>Reset password</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(PasswordReset);