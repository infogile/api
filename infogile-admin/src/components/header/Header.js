import React from "react";
import "./header.scss";
import Login from "../Login";
import {inject, observer} from "mobx-react";
import {NavLink} from "react-router-dom";

@inject("authStore")
@observer
export default class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <div className="header-body-left">
                    <div><NavLink to="/">Infogile.io</NavLink></div>
                </div>
                <div className="header-body-right">
                    <Login type="register"/>
                    <Login type="login"/>
                </div>
            </div>
        )
    }

}