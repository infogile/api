import React from "react";


export default class AuthCallback extends React.Component {

    render() {
        return (
            <div className="authcallback">
                Auth callback called!
            </div>
        )
    }

}

// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=http://localhost:8081/
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=code&client_id=638asg6skure1dukovd3raoamj&redirect_uri=http://localhost:8081/authcallback
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=https://myDomain.auth.us-east-1.amazoncognito.com