import React from "react";
import Header from "./header/Header";
import {inject, observer} from "mobx-react";

@inject("authStore")
@observer
export default class Layout extends React.Component {

    render() {
        return (
            <div>
                <div className="header-wrapper"><Header/></div>
                <div className="main-content">{this.props.children}</div>
            </div>
        )
    }

}

// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=http://localhost:8081/
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=code&client_id=6d4s6v2g5b64tere4ppjmdaocl&redirect_uri=http://localhost:8081/authcallback
// https://hallarappa-dev.auth.eu-west-1.amazoncognito.com/login?response_type=token&client_id=638asg6skure1dukovd3raoamj&redirect_uri=https://myDomain.auth.us-east-1.amazoncognito.com