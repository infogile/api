import React from 'react'
import {hot} from 'react-hot-loader'
import Login from "./components/Login";
import {Route, Switch, withRouter} from 'react-router-dom'
import AuthCallback from "./components/AuthCallback";
import PasswordReset from "./components/PasswordReset";
import Register from "./components/Register";
import VerifyEmail from "./components/VerifyEmail";
import Layout from "./components/Layout";
import Home from "./routes/Home";
import "./styles/overrides.less";
import "./styles/main.scss";
import {Provider} from "mobx-react";
//import DevTools from 'mobx-react-devtools'
import AppSpaces from "./routes/AppSpaces";
import AppSpace from "./routes/AppSpace";
import ObjectDefinitionEdit from "./routes/ObjectDefinitionEdit";
import ManageData from "./routes/ManageData";


class App extends React.Component {
    render() {
        return (
            <div>
                <Switch>
                    <Provider appSpaceStore={this.props.stores.appspace} appSpacesStore={this.props.stores.appspaces}
                              authStore={this.props.stores.auth}>
                        <Layout>
                            <Route exact path='/' component={Home}/>
                            <Route exact path='/appspaces' component={AppSpaces}/>
                            <Route exact path='/appspaces/:identity' component={AppSpace}/>
                            <Route exact path='/appspaces/:appSpaceIdentity/:objectDefinitionIdentity'
                                   component={ObjectDefinitionEdit}/>
                            <Route exact path='/data/:appSpaceIdentity/:objectDefinitionIdentity'
                                   component={ManageData}/>
                            <Route exact path="/authcallback" component={AuthCallback}/>
                            <Route path="/resetpassword/:token" component={PasswordReset}/>
                            <Route path="/register" component={Register}/>
                            <Route path="/verifyemail/:token" component={VerifyEmail}/>
                            <Route path="/signin" component={Login}/>
                        </Layout>
                    </Provider>
                </Switch>
                {/*<DevTools/>*/}
            </div>

        )
    }
}

export default hot(module)(withRouter(App))