import React from "react";
import {inject, observer} from "mobx-react/index";
import {Link, withRouter} from "react-router-dom";
import {Button, Checkbox, Dropdown, Form, Icon, Input, Menu, Select} from "antd";
import "./objectdefinitionedit.scss"
import {DragSource} from 'react-dnd';
import ObjectDefinitionFieldEdit from "../components/appspace/ObjectDefinitionFieldEdit";
import {DragDropContextProvider} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import {DragDropContext, Droppable, Draggable} from 'react-beautiful-dnd';


const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    // padding: grid * 2,
    // margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    // padding: grid,
    // width: 250,
});


@inject("authStore")
@inject("appSpaceStore")
@observer
class ObjectDefinitionEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {dragged: null, dragOver: null};
    }

    componentWillMount() {
        if (!this.props.appSpaceStore.currentObjectDefinition) {
            this.props.appSpaceStore.objectDefinition(this.props.match.params.appSpaceIdentity, this.props.match.params.objectDefinitionIdentity);
            this.props.appSpaceStore.listObjectDefinitions(this.props.match.params.appSpaceIdentity);
        }
    }

    addField() {
        if (this.props.appSpaceStore.currentObjectDefinition.fields.filter(f => f.apiName === "").length === 0) {
            this.props.appSpaceStore.currentObjectDefinition.fields.push(
                {
                    name: "",
                    apiName: "",
                    mandatory: false,
                    fieldType: "ValueField",
                    type: "Text"
                }
            )
        }
    }

    onDragEnd = (result) => {
        console.log("onDragEnd", "move", result.source.index, " to index ", result.destination.index);
        let from = result.source.index;
        let to = result.destination.index;
        let fields = this.props.appSpaceStore.currentObjectDefinition.fields;
        fields.splice(to, 0, fields.splice(from, 1)[0]);
    };

    render() {

        let c = <div>&nbsp;</div>;

        let objectDefinition = this.props.appSpaceStore.currentObjectDefinition;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 2},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 22},
            },
        };

        console.log("no of fields ", objectDefinition ? objectDefinition.fields.length : "N/A");

        let edit;
        if (objectDefinition) {
            edit = (
                <div style={{maxWidth: "1200px"}}>
                    <Form>
                        <Form.Item label="Name" {...formItemLayout}>
                            <Input value={objectDefinition.name}
                                   onChange={(e) => objectDefinition.name = e.target.value}/>
                        </Form.Item>
                        <Form.Item label="Description" {...formItemLayout}>
                            <Input value={objectDefinition.description}
                                   onChange={(e) => objectDefinition.description = e.target.value}/>
                        </Form.Item>
                        <Form.Item label="API Name" {...formItemLayout}>
                            <Input value={objectDefinition.apiName}
                                   onChange={(e) => objectDefinition.apiName = e.target.value}/>
                        </Form.Item>
                        <Form.Item label="Fields" {...formItemLayout}>
                            <div className="objectdefinition-field-container">
                                <div className="objectdefinition-field-entry header">
                                    <div className="field-input field-name">
                                        Field name
                                    </div>
                                    <div className="field-input api-name">
                                        API Name
                                    </div>
                                    <div className="field-input field-type">
                                        Field type
                                    </div>
                                    <div className="field-input value-type">
                                        Value Type/References
                                    </div>
                                    <div className="ellipsis-field">
                                    </div>
                                </div>
                                <DragDropContext onDragEnd={this.onDragEnd}>
                                    <Droppable droppableId="droppable">
                                        {(provided, snapshot) => (
                                            <div
                                                ref={provided.innerRef}
                                                style={getListStyle(snapshot.isDraggingOver)}
                                            >
                                                {objectDefinition.fields.map((f, i) => {
                                                    return (
                                                        <Draggable key={"field-" + i} draggableId={"field-" + i}
                                                                   index={i}>
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}

                                                                        style={getItemStyle(
                                                                            snapshot.isDragging,
                                                                            provided.draggableProps.style
                                                                        )}
                                                                    >
                                                                        <ObjectDefinitionFieldEdit
                                                                            position={i} id={"field-" + i}
                                                                            key={"field-" + i}
                                                                            dragHandleProps={provided.dragHandleProps}
                                                                            field={f}/>
                                                                    </div>
                                                                )
                                                            }}
                                                        </Draggable>
                                                    )
                                                })}
                                            </div>
                                        )}
                                    </Droppable>
                                </DragDropContext>
                            </div>
                        </Form.Item>
                        <Form.Item>
                            <div className="add-field-row">
                                <Button onClick={this.addField.bind(this)} icon="plus">Add field</Button>
                            </div>
                        </Form.Item>
                    </Form>
                </div>
            );
        }

        return (
            <div className="objectdefinition-edit-main">
                <div><h2>Edit Object Definition {objectDefinition ? objectDefinition.name : ""}</h2></div>
                <div>{c}</div>
                <div>
                    <Link to={"/appspaces/" + this.props.match.params.appSpaceIdentity}> &lt;&lt; Back</Link>
                </div>
                {edit}
                <div className="button-area">
                    <Button
                        onClick={() => this.props.history.push("/appspaces/" + this.props.match.params.appSpaceIdentity)}>Cancel</Button>
                    <Button type="primary"
                            loading={this.props.appSpaceStore.objectDefinitionInSaveProgress}
                            onClick={() => this.props.appSpaceStore.saveCurrentObjectDefinition()}>Save</Button>
                </div>
            </div>
        )
    }

}

export default withRouter(ObjectDefinitionEdit);