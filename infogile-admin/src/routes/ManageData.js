import React from "react";
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import InfogileDataTable from "../components/data/InfogileDataTable";
import DevTools from "mobx-react-devtools";
import {Icon, Input} from "antd";

@inject("authStore")
@inject("appSpaceStore")
@observer
class ManageData extends React.Component {

    componentWillMount() {
        this.search();
    }

    search(filter) {
        this.props.appSpaceStore.objectDefinitionData(this.props.match.params.appSpaceIdentity, this.props.match.params.objectDefinitionIdentity, filter);
    }

    render() {
        const Search = Input.Search;

        let data = this.props.appSpaceStore.currentObjectDefinitionData || {};
        let objectDefinitionName;
        if (data && data.metadata && data.metadata.objectDefinition) {
            objectDefinitionName = data.metadata.objectDefinition.name;
        }

        return (
            <div>
                <div><Link to="/appspaces">AppSpaces</Link> &gt; <Link
                    to={"/appspaces/" + this.props.match.params.appSpaceIdentity}>AppSpace</Link> &gt; {objectDefinitionName}
                </div>
                <h2>Manage data for {objectDefinitionName}</h2>
                <div><Link to={"/appspaces/" + this.props.match.params.appSpaceIdentity}><Icon type="backward"/>Back to
                    Object Definitions</Link></div>
                <div>
                    <Search
                        enterButton
                        onSearch={value => this.search(value)}
                        placeholder="Structured query: column = 'value' or column like 'valu'"/>
                    <InfogileDataTable data={data}/>
                    <DevTools/>
                </div>
            </div>
        )
    }
}

export default withRouter(ManageData);