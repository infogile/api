import React from "react";
import {inject, observer} from "mobx-react/index";
import {Link, withRouter} from "react-router-dom";
import AppSpaces from "./AppSpaces";


@inject("authStore")
@observer
class Home extends React.Component {
    render() {
        let c = <div>&nbsp;</div>;
        let appspaces;
        if (this.props.authStore.user) {
            console.log("Yep, logged in!");
            c = null;
            appspaces = <AppSpaces/>
        } else {
            appspaces = <div>Please login to view your appspaces</div>
        }

        return (
            <div className="main">
                {appspaces}
            </div>
        )
    }

}

export default withRouter(Home);