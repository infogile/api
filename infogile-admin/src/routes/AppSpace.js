import React from "react";
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import ObjectDefinitionCardList from "../components/appspace/ObjectDefinitionCardList";
import {Button} from "antd";


@inject("authStore")
@inject("appSpaceStore")
@observer
class AppSpace extends React.Component {

    constructor(props) {
        super(props);
    }


    componentWillMount() {
        console.log("match: ", this.props.match.params.identity);
        this.props.appSpaceStore.listObjectDefinitions(this.props.match.params.identity);
    }

    createDummy() {
        console.log("appSpaceIdentity: ", this.props.match.params.identity);
        this.props.appSpaceStore.createDummy(this.props.match.params.identity);
    }

    render() {
        let appSpaceStore = this.props.appSpaceStore;
        console.log(appSpaceStore);
        return (
            <div>
                <div className=""><Link to="/appspaces">AppSpaces</Link> &gt; AppSpace</div>

                <div>
                    <Button onClick={() => {
                        appSpaceStore.newObjectDefinition(this.props.match.params.identity);
                        this.props.history.push("/appspaces/" + this.props.match.params.identity + "/new")
                    }}>Create Object Definition</Button>
                </div>
                <div>
                    <ObjectDefinitionCardList objectDefinitions={appSpaceStore.objectDefinitions}/>
                </div>
            </div>
        )
    }
}

export default withRouter(AppSpace);