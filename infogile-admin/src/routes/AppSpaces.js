import React from "react";
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import {Button, Card, Icon, Input, Modal} from "antd";

@inject("authStore")
@inject("appSpacesStore")
@observer
class AppSpaces extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({appSpaces: [], initiated: false, createModal: false, newAppSpaceName: ""})
    }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     console.log("componentDidUpdate", prevProps.authStore.user, snapshot);
    // if (!prevProps.authStore.user && this.props.authStore.user) {
    //     this.getAppSpaces();
    // }
    // }

    componentDidMount() {
        if (this.props.authStore.user && !this.state.initiated) {
            this.getAppSpaces();
        }
    }

    getAppSpaces() {
        if (!this.initiated) {
            this.initiated = true;
            console.log("user", this.props.authStore.user);
            this.props.appSpacesStore.listAppSpaces({
                accessToken: this.props.authStore.user.accessToken,
                identityProvider: this.props.authStore.user.identityProvider
            });
        }
    }

    createAppSpace() {
        this.props.appSpacesStore.createAppSpace(this.state.newAppSpaceName, {
            accessToken: this.props.authStore.user.accessToken,
            identityProvider: this.props.authStore.user.identityProvider
        });
        this.setState({createModal: false});
    }

    showCreateModal() {
        this.setState({createModal: true});
    }

    hideCreateModal() {
        this.setState({createModal: false, newAppSpaceName: ""})
    }


    render() {
        let user = this.props.authStore.user;
        if (!user) {
            return <div>Please login to manage AppSpaces</div>;
        }
        let createModal;
        if (this.state.createModal === true) {
            createModal = (
                <Modal
                    title="New AppSpace"
                    visible={this.state.createModal === true}
                    onOk={this.createAppSpace.bind(this)}
                    onCancel={this.hideCreateModal.bind(this)}
                    footer={[
                        <Button key="back" onClick={this.hideCreateModal.bind(this)}>Cancel</Button>,
                        <Button key="ok" onClick={this.createAppSpace.bind(this)}>Create</Button>
                    ]}
                >
                    <Input onChange={e => this.setState({newAppSpaceName: e.target.value})}
                           placeholder="Name of new AppSpace"
                    />
                </Modal>
            )
        }
        return (
            <div>
                <div>AppSpaces list</div>
                <h2>Appspace list</h2>
                <div>
                    <Button onClick={this.showCreateModal.bind(this)}>Create AppSpace</Button>
                    {createModal}
                </div>
                <div className="card-container">
                    {this.props.appSpacesStore.appSpaces.map(as => {
                            let adminAccess = as.access.includes("ADMIN");
                            let colorStyle = {};
                            if (!adminAccess) {
                                colorStyle = {color: "#dddddd"};
                            }
                            return (
                                <Card key={as.appSpace.identity}
                                      actions={[<Icon style={colorStyle} type="setting"/>,
                                          <Link to={"/appspaces/" + as.appSpace.identity}><Icon type="edit"/></Link>]}
                                      title={as.appSpace.name}
                                      style={{width: 350}}>
                                    <p>{as.appSpace.description || <i>No description</i>}</p>
                                    <table>
                                        <tr>
                                            <td>Created</td>
                                            <td>{as.appSpace.createdDate}</td>
                                        </tr>
                                        <tr>
                                            <td>Created by</td>
                                            <td>{as.appSpace.createdBy}</td>
                                        </tr>
                                    </table>
                                </Card>
                            )
                        }
                    )}
                </div>
            </div>
        )
    }
}

export default withRouter(AppSpaces);