const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "production",
    devtool: 'source-map',
    node: {
        tls: "empty",
        fs: "empty"
    },
    entry: {
        app: './src/index.js'
    },
    optimization: {
        minimizer: [new TerserPlugin({
            parallel: true,
            sourceMap: true,
        })],
    },
    plugins: [
        // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Production'
        })
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                exclude: /node_modules|packages/,
                test: /\.js$/,
                use: 'babel-loader',
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                loader: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.less/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.less/,
                loader: 'less-loader',
                options: {
                    javascriptEnabled: true,
                }
            },
            {
                test: /\.svg/,
                loader: 'svg-react-loader'
            }
        ]
    }
};