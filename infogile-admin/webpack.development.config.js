const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['./src/index'],
    output: {
        path: path.join(__dirname, '/'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    mode: 'development',
    devServer: {
        proxy: {
            // '/api': {
            //     target: 'https://infogile.io',
            //     changeOrigin: true
            // },
            // '/auth': {
            //     target: 'https://infogile.io',
            //     changeOrigin: true
            // },
            '/api': {
                target: 'http://localhost:8080',
                changeOrigin: true
            },
            '/auth': {
                target: 'http://localhost:9000',
                changeOrigin: true,
            }
        },
        historyApiFallback: true,
        port: 3000,
    },
    module: {
        rules: [
            {
                exclude: /node_modules|packages/,
                test: /\.js$/,
                use: 'babel-loader',
            },
            {
                test: /\.js?$/,
                include: /node_modules/,
                use: ['react-hot-loader/webpack'],
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                loader: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.less/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.less/,
                loader: 'less-loader',
                options: {
                    javascriptEnabled: true,
                }
            },
            {
                test: /\.svg/,
                loader: 'svg-react-loader'
            },
            // {
            //     test: /\.json$/,
            //     loader: 'json-loader'
            // }
        ],
    },
    plugins: [new HtmlWebpackPlugin({
        title: 'My App',
        template: 'src/index.html'
    }), new webpack.NamedModulesPlugin()],
    resolve: {
        alias: {
            'react-hot-loader': path.resolve(path.join(__dirname, './node_modules/react-hot-loader')),
            // add these 2 lines below so linked package will reference the patched version of `react` and `react-dom`
            'react': path.resolve(path.join(__dirname, './node_modules/react')),
            'react-dom': path.resolve(path.join(__dirname, './node_modules/react-dom')),
            // or point react-dom above to './node_modules/@hot-loader/react-dom' if you are using React-🔥-Dom
        }
    }
};
