# Infogile API
This repository contains a reference implementation of how to use the Infogile API. Namely
the admin UI found att [infogile.io](https://infogile.io).

For more detailed documentation, please see the [Wiki](https://gitlab.com/infogile/api/wikis/home)

## The client
The client is a UI built using React, React Router and MobX. It is not intended
to be a model React project (we're more into working with cool distributed persistence solutions)
but it *is* the administrative UI for Infogile so it should work.

### Prerequisities
Node.js and Yarn

### Building the code
1. Clone the repository: `git clone https://gitlab.com/infogile/api.git`
2. Run `yarn install`
3. Run `yarn start` to start the app in development mode
4. Visit [http://localhost:3000](http://localhost:3000)

## Some pointers

- Authentication, appspaces and user info is available in [src/api/auth/index.js](infogile-admin/src/api/auth/index.js)
- Structure and data api is implemented in [src/api/infogile/index.js](infogile-admin/src/api/infogile/index.js)