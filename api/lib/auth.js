"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAppSpace = exports.getAppSpaces = exports.getUser = exports.register = void 0;

var _superagent = _interopRequireDefault(require("superagent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var register = function register(idToken) {
  var opts = {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + idToken
    }
  };
  return new Promise(function (resolve, reject) {
    fetch("/auth/register", opts).then(function (res) {
      if (res.status >= 200 && res.status < 300) {
        if (res.headers.get("accessToken")) {
          self.session = res.headers.get("accessToken");
        }

        resolve(res.json());
      } else {
        if (res.status === 401 && self.session) {
          self.session = null;
          reject({
            status: 401
          });
        }
      }
    })["catch"](function (err) {
      if (err.status === 401 && self.session) {
        self.session = null;
      }

      reject(err);
    });
  });
};

exports.register = register;

var getUser = function getUser(identityProvider, accessToken) {
  return new Promise(function (resolve, reject) {
    if (identityProvider == null || accessToken === null) {
      reject("IdentityProvider and AccessToken must be provided!");
      return;
    }

    _superagent["default"].post("/auth/userinfo").set("Authorization", "Bearer " + accessToken).set("IdentityProvider", identityProvider).then(function (res, err) {
      if (err) {
        reject(err);
        return;
      }

      console.log("Res: ", res);
      resolve(res.body);
    })["catch"](function (e) {
      reject(e);
    });
  });
};

exports.getUser = getUser;

var getAppSpaces = function getAppSpaces(authenticationOptions) {
  var opts = {
    method: "GET",
    headers: {
      "Authorization": "Bearer " + authenticationOptions.accessToken,
      "IdentityProvider": authenticationOptions.identityProvider
    }
  };
  return new Promise(function (resolve, reject) {
    fetch("/auth/appspaces", opts).then(function (res) {
      if (res.status >= 200 && res.status < 300) {
        if (res.headers.get("accessToken")) {
          self.session = res.headers.get("accessToken");
        }

        resolve(res.json());
      } else {
        if (res.status === 401 && self.session) {
          self.session = null;
          reject({
            status: 401
          });
        }
      }
    })["catch"](function (err) {
      if (err.status === 401 && self.session) {
        self.session = null;
      }

      reject(err);
    });
  });
};

exports.getAppSpaces = getAppSpaces;

var createAppSpace = function createAppSpace(name, authenticationOptions) {
  var opts = {
    method: "POST",
    headers: {
      "Authorization": "Bearer " + authenticationOptions.accessToken,
      "IdentityProvider": authenticationOptions.identityProvider,
      "Content-Type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({
      name: name
    })
  };
  console.log("Creating appspace with request", opts);
  return new Promise(function (resolve, reject) {
    fetch("/auth/appspace", opts).then(function (res) {
      if (res.status >= 200 && res.status < 300) {
        if (res.headers.get("accessToken")) {
          self.session = res.headers.get("accessToken");
        }

        resolve(res.json());
      } else {
        if (res.status === 401 && self.session) {
          self.session = null;
          reject({
            status: 401
          });
        }
      }
    })["catch"](function (err) {
      if (err.status === 401 && self.session) {
        self.session = null;
      }

      reject(err);
    });
  });
};

exports.createAppSpace = createAppSpace;