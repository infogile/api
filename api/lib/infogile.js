"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Infogile =
/*#__PURE__*/
function () {
  function Infogile() {
    _classCallCheck(this, Infogile);
  }

  _createClass(Infogile, null, [{
    key: "init",
    value: function init(options) {
      if (options) {
        this.idTokenProvider = options.idTokenProvider;
      }

      if (this.sessionTimeStart < Date.now() - 300000) {
        this.sessionTimeStart = Date.now();
        return this.login();
      } else {
        return new Promise(function (resolve) {
          resolve();
        });
      }
    }
  }, {
    key: "login",
    value: function login() {
      return this.call("/auth/validlogin", {
        method: "GET"
      });
    }
  }, {
    key: "uid",
    value: function uid() {
      return this.call("/api/uid", {
        method: "GET"
      });
    }
  }, {
    key: "objectDefinitions",
    value: function objectDefinitions(appSpaceIdentity) {
      return this.call("/api/" + appSpaceIdentity + "/objectdefinition", {
        method: "GET"
      });
    }
  }, {
    key: "objectDefinition",
    value: function objectDefinition(appSpaceIdentity, objectDefinitionIdentity) {
      return this.call("/api/" + appSpaceIdentity + "/objectdefinition/" + objectDefinitionIdentity, {
        method: "GET"
      });
    }
  }, {
    key: "saveObjectDefinition",
    value: function saveObjectDefinition(appSpaceIdentity, objectDefinition) {
      return this.call("/api/" + appSpaceIdentity + "/objectdefinition", {
        method: "PUT",
        body: JSON.stringify(objectDefinition)
      });
    }
  }, {
    key: "deleteObjectDefinition",
    value: function deleteObjectDefinition(appSpaceIdentity, identity) {
      return this.call("/api/" + appSpaceIdentity + "/objectdefinition/" + identity, {
        method: "DELETE"
      });
    }
  }, {
    key: "listData",
    value: function listData(options) {
      var request = {
        start: options.start || 0,
        offset: options.offset || 10000
      };

      if (options.filter) {
        request.qlFilter = options.filter;
      }

      return this.call("/api/" + options.appSpaceIdentity + "/item/" + options.objectDefinitionIdentity, {
        method: "POST",
        body: JSON.stringify(request)
      });
    }
  }, {
    key: "saveItem",
    value: function saveItem(appSpaceIdentity, item) {
      return this.call("/api/" + appSpaceIdentity + "/item", {
        method: "PUT",
        body: JSON.stringify(item)
      });
    }
  }, {
    key: "deleteItems",
    value: function deleteItems(appSpaceIdentity) {
      for (var _len = arguments.length, items = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        items[_key - 1] = arguments[_key];
      }

      return this.call("/api/" + appSpaceIdentity + "/item/", {
        method: "DELETE",
        body: JSON.stringify(items.map(function (i) {
          return i.identity;
        }))
      });
    }
  }, {
    key: "openWebsocket",
    value: function openWebsocket(callback) {
      var _this = this;

      var websocket = new WebSocket("ws://localhost:8080/subscribe");
      window.subscription = websocket;
      callback(websocket);

      websocket.onmessage = function (msg) {
        console.log("Message: ", msg);
        var msgObject = JSON.parse(msg.data);
        console.log("msgObject", msgObject);
        console.log("changeListeners", _this.changeListeners);

        if (msgObject.appSpaceIdentity && msgObject.objectDefinitionIdentity) {
          var listeners = _this.changeListeners[msgObject.appSpaceIdentity.toUpperCase() + msgObject.objectDefinitionIdentity];

          console.log("listeners", listeners);

          if (listeners && listeners.length > 0) {
            listeners.forEach(function (l) {
              return l(msgObject);
            });
          }
        }
      };

      websocket.close = function () {
        window.subscription = null;
      };
    } // static subscribe()

  }, {
    key: "createDummyObjectDefinition",
    value: function createDummyObjectDefinition(appSpaceIdentity) {
      var objectDefinition = {
        "apiName": "assignment",
        "name": "Assignment",
        "description": "A very simple assignment",
        "fields": [{
          "apiName": "summary",
          "name": "Summary",
          "description": "The summary of the assignment",
          "type": "Text",
          "mandatory": true,
          "fieldType": "ValueField"
        }, {
          "apiName": "description",
          "name": "Description",
          "description": "The description of the assignment",
          "type": "Text",
          "mandatory": true,
          "fieldType": "ValueField"
        }, {
          "apiName": "assignee",
          "name": "Assignee",
          "description": "The assignee of the task",
          "referencedType": "user",
          "mandatory": false,
          "fieldType": "ReferenceField",
          "multiplicity": "OneToOne"
        }]
      }; // objectDefinition = {
      //     "apiName": "religion",
      //     "name": "Religion",
      //     "description": "What religion a person can believe in",
      //     "fields": [
      //         {
      //             "apiName": "religion",
      //             "name": "Religion",
      //             "description": "The religion",
      //             "type": "String",
      //             "mandatory": true,
      //             "fieldType": "ValueField"
      //         }
      //     ]
      // };

      return this.call("/api/" + appSpaceIdentity + "/objectDefinition", {
        method: "PUT",
        body: JSON.stringify(objectDefinition)
      });
    }
  }, {
    key: "call",
    value: function () {
      var _call = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(url, options, isAccessDeniedRetry) {
        var _this2 = this;

        var self;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                self = this;
                return _context3.abrupt("return", new Promise(
                /*#__PURE__*/
                function () {
                  var _ref = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee2(resolve, reject) {
                    var opts, _options;

                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            opts = Object.assign({}, options);

                            if (!opts.headers) {
                              opts.headers = {};
                            }

                            opts.headers["Content-Type"] = "application/json;charset=UTF-8"; // if (this.session) {
                            //     opts.headers["InfogileSessionId"] = this.session;
                            // } else

                            if (!_this2.idTokenProvider) {
                              _context2.next = 11;
                              break;
                            }

                            _context2.next = 6;
                            return _this2.idTokenProvider();

                          case 6:
                            _options = _context2.sent;
                            opts.headers["Authorization"] = "Bearer " + _options.accessToken;
                            opts.headers["IdentityProvider"] = _options.identityProvider;
                            _context2.next = 12;
                            break;

                          case 11:
                            return _context2.abrupt("return", new Promise(function (resolve, reject) {
                              reject({
                                status: 401,
                                description: "Unauthorizied"
                              });
                            }));

                          case 12:
                            // if (this.appSpaceIdentity) {
                            //     opts.headers["AppSpaceIdentity"] = this.appSpaceIdentity;
                            // }
                            fetch(url, opts).then(function (res) {
                              return {
                                text: res.text(),
                                status: res.status
                              };
                            }).then(
                            /*#__PURE__*/
                            function () {
                              var _ref2 = _asyncToGenerator(
                              /*#__PURE__*/
                              regeneratorRuntime.mark(function _callee(res) {
                                var text, object;
                                return regeneratorRuntime.wrap(function _callee$(_context) {
                                  while (1) {
                                    switch (_context.prev = _context.next) {
                                      case 0:
                                        if (!(res.status >= 200 && res.status < 300)) {
                                          _context.next = 7;
                                          break;
                                        }

                                        _context.next = 3;
                                        return res.text;

                                      case 3:
                                        text = _context.sent;

                                        if (text.length > 0) {
                                          object = JSON.parse(text);
                                          resolve(object);
                                        } else {
                                          resolve();
                                        }

                                        _context.next = 9;
                                        break;

                                      case 7:
                                        if (res.status === 401 && self.session) {
                                          self.session = null;
                                        }

                                        reject({
                                          status: res.status
                                        });

                                      case 9:
                                      case "end":
                                        return _context.stop();
                                    }
                                  }
                                }, _callee);
                              }));

                              return function (_x6) {
                                return _ref2.apply(this, arguments);
                              };
                            }())["catch"](function (err) {
                              if (err.status === 401 && self.session) {
                                self.session = null;
                              }

                              reject(err);
                            });

                          case 13:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2);
                  }));

                  return function (_x4, _x5) {
                    return _ref.apply(this, arguments);
                  };
                }()));

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function call(_x, _x2, _x3) {
        return _call.apply(this, arguments);
      }

      return call;
    }()
  }, {
    key: "isLoggedIn",
    value: function isLoggedIn() {
      return this.session != null;
    }
  }]);

  return Infogile;
}();

exports["default"] = Infogile;
Infogile.idToken = null;
Infogile.idTokenProvider = null;
Infogile.session = null;
Infogile.sessionTimeStart = 0;
Infogile.changeListeners = {};

Infogile.subscribe = function (appSpace, objectDefinition, listener) {
  var promise = new Promise(function (resolve, reject) {
    if (!window.subscription) {
      Infogile.openWebsocket(function (websocket) {
        window.subscription = websocket;
        resolve(websocket);
      });
    } else {
      resolve(window.subscription);
    }
  });
  var idTokenProvider = Infogile.idTokenProvider;
  return new Promise(function (resolve, reject) {
    promise.then(function (websocket) {
      if (idTokenProvider) {
        if (!Infogile.changeListeners[appSpace + objectDefinition] || Infogile.changeListeners[appSpace + objectDefinition].length === 0) {
          var token = idTokenProvider();
          console.log("sending subscribe request to websocket");
          websocket.send(JSON.stringify({
            token: token.accessToken,
            objectDefinitionIdentity: objectDefinition,
            appSpaceIdentity: appSpace
          }));

          if (!Infogile.changeListeners[appSpace + objectDefinition]) {
            Infogile.changeListeners[appSpace + objectDefinition] = [];
          }

          Infogile.changeListeners[appSpace + objectDefinition].push(listener);
        }

        resolve();
      } else {
        reject("couldn't send subscribe request!");
      }
    });
  });
};