export default class Infogile {
    static idToken = null;
    static idTokenProvider = null;
    static session = null;
    static sessionTimeStart = 0;
    static changeListeners = {};

    static init(options) {
        if (options) {
            this.idTokenProvider = options.idTokenProvider;
        }
        if (this.sessionTimeStart < (Date.now() - 300000)) {
            this.sessionTimeStart = Date.now();
            return this.login()
        } else {
            return new Promise((resolve) => {
                resolve();
            });
        }
    }


    static login() {
        return this.call("/auth/validlogin", {
            method: "GET"
        });
    }

    static uid() {
        return this.call("/api/uid", {
            method: "GET"
        });
    }

    static objectDefinitions(appSpaceIdentity) {
        return this.call("/api/"+appSpaceIdentity+"/objectdefinition", {
            method: "GET"
        });
    }

    static objectDefinition(appSpaceIdentity, objectDefinitionIdentity) {
        return this.call("/api/" + appSpaceIdentity + "/objectdefinition/" + objectDefinitionIdentity, {
            method: "GET"
        });
    }

    static saveObjectDefinition(appSpaceIdentity, objectDefinition) {
        return this.call("/api/" + appSpaceIdentity + "/objectdefinition", {
            method: "PUT",
            body: JSON.stringify(objectDefinition)
        });
    }

    static deleteObjectDefinition(appSpaceIdentity, identity) {
        return this.call("/api/" + appSpaceIdentity + "/objectdefinition/" + identity, {
            method: "DELETE"
        });
    }

    static listData(options) {
        let request = {
            start: options.start || 0,
            offset: options.offset || 10000
        };
        if (options.filter) {
            request.qlFilter = options.filter;
        }
        return this.call("/api/"+options.appSpaceIdentity+"/item/" + options.objectDefinitionIdentity, {
            method: "POST",
            body: JSON.stringify(request)
        });
    }

    static saveItem(appSpaceIdentity, item) {
        return this.call("/api/" + appSpaceIdentity + "/item", {
            method: "PUT",
            body: JSON.stringify(item)
        });
    }

    static deleteItems(appSpaceIdentity, ...items) {
        return this.call("/api/" + appSpaceIdentity + "/item/", {
            method: "DELETE",
            body: JSON.stringify(items.map(i => i.identity))
        });
    }

    static openWebsocket(callback) {
        let websocket = new WebSocket("ws://localhost:8080/subscribe");
        window.subscription = websocket;
        callback(websocket);
        websocket.onmessage = msg => {
            console.log("Message: ", msg);
            let msgObject = JSON.parse(msg.data);
            console.log("msgObject", msgObject);
            console.log("changeListeners", this.changeListeners);
            if (msgObject.appSpaceIdentity && msgObject.objectDefinitionIdentity) {
                let listeners = this.changeListeners[msgObject.appSpaceIdentity.toUpperCase() + msgObject.objectDefinitionIdentity];
                console.log("listeners", listeners);
                if (listeners && listeners.length > 0) {
                    listeners.forEach(l => l(msgObject));
                }
            }
        };
        websocket.close = () => {
            window.subscription = null;
        }
    }

    // static subscribe()


    static subscribe = (appSpace, objectDefinition, listener) => {
        let promise = new Promise((resolve, reject) => {
            if (!window.subscription) {
                Infogile.openWebsocket((websocket) => {
                    window.subscription = websocket;
                    resolve(websocket);
                })
            } else {
                resolve(window.subscription);
            }
        });
        let idTokenProvider = Infogile.idTokenProvider;
        return new Promise((resolve, reject) => {
            promise.then(websocket => {
                if (idTokenProvider) {
                    if (!Infogile.changeListeners[appSpace + objectDefinition] || Infogile.changeListeners[appSpace + objectDefinition].length === 0) {
                        let token = idTokenProvider();
                        console.log("sending subscribe request to websocket")
                        websocket.send(
                            JSON.stringify({
                                token: token.accessToken,
                                objectDefinitionIdentity: objectDefinition,
                                appSpaceIdentity: appSpace
                            })
                        );
                        if (!Infogile.changeListeners[appSpace + objectDefinition]) {
                            Infogile.changeListeners[appSpace + objectDefinition] = [];
                        }
                        Infogile.changeListeners[appSpace + objectDefinition].push(listener);
                    }
                    resolve();

                } else {
                    reject("couldn't send subscribe request!");
                }

            })
        })
    };

    static createDummyObjectDefinition(appSpaceIdentity) {
        let objectDefinition = {
            "apiName": "assignment",
            "name": "Assignment",
            "description": "A very simple assignment",
            "fields": [
                {
                    "apiName": "summary",
                    "name": "Summary",
                    "description": "The summary of the assignment",
                    "type": "Text",
                    "mandatory": true,
                    "fieldType": "ValueField"
                },
                {
                    "apiName": "description",
                    "name": "Description",
                    "description": "The description of the assignment",
                    "type": "Text",
                    "mandatory": true,
                    "fieldType": "ValueField"
                },
                {
                    "apiName": "assignee",
                    "name": "Assignee",
                    "description": "The assignee of the task",
                    "referencedType": "user",
                    "mandatory": false,
                    "fieldType": "ReferenceField",
                    "multiplicity": "OneToOne"
                },
            ]
        };

        // objectDefinition = {
        //     "apiName": "religion",
        //     "name": "Religion",
        //     "description": "What religion a person can believe in",
        //     "fields": [
        //         {
        //             "apiName": "religion",
        //             "name": "Religion",
        //             "description": "The religion",
        //             "type": "String",
        //             "mandatory": true,
        //             "fieldType": "ValueField"
        //         }
        //     ]
        // };
        return this.call("/api/" + appSpaceIdentity + "/objectDefinition", {
            method: "PUT",
            body: JSON.stringify(objectDefinition)
        });


    }

    static async call(url, options, isAccessDeniedRetry) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            let opts = Object.assign({}, options);

            if (!opts.headers) {
                opts.headers = {};
            }

            opts.headers["Content-Type"] = "application/json;charset=UTF-8";

            // if (this.session) {
            //     opts.headers["InfogileSessionId"] = this.session;
            // } else
            if (this.idTokenProvider) {
                let options = await this.idTokenProvider();
                opts.headers["Authorization"] = "Bearer " + options.accessToken;
                opts.headers["IdentityProvider"] = options.identityProvider;
            } else {
                return new Promise((resolve, reject) => {
                    reject({status: 401, description: "Unauthorizied"})
                })
            }
            // if (this.appSpaceIdentity) {
            //     opts.headers["AppSpaceIdentity"] = this.appSpaceIdentity;
            // }

            fetch(url, opts)
                .then(res => {
                    return {
                        text: res.text(),
                        status: res.status
                    }
                })
                .then(async res => {
                    if (res.status >= 200 && res.status < 300) {
                        // if (res.headers.get("infogilesessionid")) {
                        //     self.session = res.headers.get("infogilesessionid");
                        // }
                        let text = await res.text;
                        if (text.length > 0) {
                            let object = JSON.parse(text);
                            resolve(object);
                        } else {
                            resolve();
                        }
                    } else {
                        if (res.status === 401 && self.session) {
                            self.session = null;
                        }
                        reject({status: res.status})
                    }
                })
                .catch(err => {
                    if (err.status === 401 && self.session) {
                        self.session = null;
                    }
                    reject(err);
                })

        });
    }

    static isLoggedIn() {
        return this.session != null;
    }
}