import request from "superagent";

export const register = (idToken) => {
    let opts = {
        method: "POST",
        headers: {
            "Authorization": "Bearer " + idToken
        }
    };
    return new Promise(((resolve, reject) => {
        fetch("/auth/register", opts)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    if (res.headers.get("accessToken")) {
                        self.session = res.headers.get("accessToken");
                    }
                    resolve(res.json());
                } else {
                    if (res.status === 401 && self.session) {
                        self.session = null;
                        reject({status: 401})
                    }
                }
            })
            .catch(err => {
                if (err.status === 401 && self.session) {
                    self.session = null;
                }
                reject(err);
            })
    }));
};

export const getUser = (identityProvider, accessToken) => {
    return new Promise((resolve, reject) => {
        if (identityProvider == null || accessToken === null) {
            reject("IdentityProvider and AccessToken must be provided!");
            return;
        }
        request.post("/auth/userinfo")
            .set("Authorization", "Bearer " + accessToken)
            .set("IdentityProvider", identityProvider)
            .then((res, err) => {
                if (err) {
                    reject(err);
                    return;
                }
                console.log("Res: ", res);
                resolve(res.body);
            })
            .catch(e => {
                reject(e)
            });
    });
};


export const getAppSpaces = (authenticationOptions) => {
    let opts = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + authenticationOptions.accessToken,
            "IdentityProvider": authenticationOptions.identityProvider
        }
    };
    return new Promise(((resolve, reject) => {
        fetch("/auth/appspaces", opts)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    if (res.headers.get("accessToken")) {
                        self.session = res.headers.get("accessToken");
                    }
                    resolve(res.json());
                } else {
                    if (res.status === 401 && self.session) {
                        self.session = null;
                        reject({status: 401})
                    }
                }
            })
            .catch(err => {
                if (err.status === 401 && self.session) {
                    self.session = null;
                }
                reject(err);
            })
    }));
};

export const createAppSpace = (name, authenticationOptions) => {
    let opts = {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + authenticationOptions.accessToken,
                "IdentityProvider": authenticationOptions.identityProvider,
                "Content-Type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify({
                name: name
            })
        }
    ;

    console.log("Creating appspace with request", opts);

    return new Promise(((resolve, reject) => {
        fetch("/auth/appspace", opts)
            .then(res => {
                if (res.status >= 200 && res.status < 300) {
                    if (res.headers.get("accessToken")) {
                        self.session = res.headers.get("accessToken");
                    }
                    resolve(res.json());
                } else {
                    if (res.status === 401 && self.session) {
                        self.session = null;
                        reject({status: 401})
                    }
                }
            })
            .catch(err => {
                if (err.status === 401 && self.session) {
                    self.session = null;
                }
                reject(err);
            })
    }));
};